$(document).ready(function() {
    "use strict";
    $('.ipsAutosave').ipAsdModuleInlineManagementTranslate();
});


(function($) {
    "use strict";
    var methods = {init: function(options) {
            return this.each(function() {
                var $this = $(this);
                var data = $this.data('ipInlineManagementText');

                if (!data) {
                    $this.data('ipInlineManagementText', {translate: $this.data('translate'), type: $this.data('type'), name: $this.data('name'), language: $this.data('language')});
                    var translate = $this.data('ipInlineManagementText').translate;
                    var type = $this.data('ipInlineManagementText').type;
                    var name = $this.data('ipInlineManagementText').name;
                    var language = $this.data('ipInlineManagementText').language;

                    $this.parent('td').on( 'click', '.ipsSave', function(e) {
                        var el = $(this);
                        save( $this.val(), translate, type, name, language);
                        el.addClass('disabled');
                        setTimeout(function() { el.removeClass('disabled'); }, 500 );
                    });
                }
            });
        }};
    var save = function(html, translate, type, name, language) {
        var $this = $(this);
        var data = Object();
        data.aa = 'AsdTranslate.saveTranslation';
        data.securityToken = ip.securityToken;
        data.translate = translate;
        data.type = type;
        data.name = name;
        data.language = language;
        data['translation'] = html;
        $.ajax({type: 'POST', url: ip.baseUrl, data: data, context: $this, success: function() {}, dataType: 'json'});
    };
    
    $.fn.ipAsdModuleInlineManagementTranslate = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.ipInlineManagementText');
        }
    };
})(jQuery);
